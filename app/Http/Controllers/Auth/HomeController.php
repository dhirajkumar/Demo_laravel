<?php

namespace App\Http\Controllers\Auth;
use View;
use App\student;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

   public function get()
    {
       $porto = student::all();
	    $content = json_encode($porto, JSON_PRETTY_PRINT);
		//echo "<pre>".$content."</pre>";
		echo $content;
      // print_r($porto);die();


       //return view('show')->with(['portos' => $porto]);
       //return view('show');

    }
	
	public function post($name)
	{
		
	}
}
