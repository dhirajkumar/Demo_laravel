<?php

namespace App\Http\Controllers\Auth;
use App\student;
use View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		
        $data = student::all();
		//echo $data[0]["Name"];
		
        
		$content = json_encode($data, JSON_PRETTY_PRINT);
        //$studentName = $data[0]["Name"];
        //$studentAddress = $data["Address"];

        //$responseData = json_encode(array('name'=>$studentName,'address'=>$studentAddress));
        //return $responseData ;
        return $content ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		echo "hello";
		// return View::make('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//$input = Input::all();
		//echo $input;
        $data = $request->all();
		//echo $data["Name"];
        $studentName = $data["Name"];
        $studentAddress = $data["Address"];
		student::create([
                    'Name' => $studentName,
                    'Address' => $studentAddress ,
                ]);

        $responseData = json_encode(array('name'=>$studentName,'address'=>$studentAddress));
        return $responseData;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
