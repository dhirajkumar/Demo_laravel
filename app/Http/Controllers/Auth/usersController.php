<?php

namespace App\Http\Controllers\Auth;
use View;
use App\users;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class usersController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

   public function index()
    {
        $porto = users::all();
	    $content = json_encode($porto, JSON_PRETTY_PRINT);
		echo $content;
     

    }
	
	public function store()
	{
		$input = Input::all();
		users::create([
			'name' => $Input['name'];
		]);
	}
}
