<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    //public $table = "student";
     public $table = 'users';
	 
	  protected $fillable = [
        'name'
    ];
}
